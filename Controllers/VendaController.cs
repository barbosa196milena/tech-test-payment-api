using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Entities;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController : Controller
    {
        List<Venda> vendasGerais = new List<Venda>();

        [HttpPost]
        public IActionResult RegistraVenda(RegistraVenda registrarVenda)
        {
            Venda venda = new Venda();
            Console.Write(vendasGerais.Count);
            if (vendasGerais.Count == 0)
            {
                venda.IdPedido = 1;
            }
            else
            {
                venda.IdPedido = vendasGerais.Max(venda => venda.IdPedido) + 1;
            }

            venda.Vendendor = registrarVenda.Vendendor;
            venda.Produto = registrarVenda.Produto;
            venda.Status = ((int)Status.AguardandoPagamento);
            venda.Data = DateTime.Now;

            vendasGerais.Add(venda);

            return Ok(venda);
        }


        [HttpGet("{id}")]
        public IActionResult BuscaVenda(int id)
        {
            var venda = vendasGerais.Find(venda => venda.IdPedido == id);

            if (venda == null)
                return NotFound("Venda não encontrada");

            return Ok(venda);
        }

        [HttpPut("{id}")]
        public IActionResult AtualizarStatusVenda(int id, Status statusAtualizacao)
        {

            var vendaAtualizar = vendasGerais.First(venda => venda.IdPedido == id);

            if (ValidaAtualizacaoStatus(statusAtualizacao, (Status)vendaAtualizar.Status))
            {
                vendaAtualizar.Status = (int)statusAtualizacao;
            }
            else
            {
               return BadRequest(@$"Não é possível atualizar para o status {statusAtualizacao}, status da venda é {vendaAtualizar.Status}");
            }

            return Ok(@$"Status da venda {id} atualizado para {statusAtualizacao} com sucesso!");
        }


        bool ValidaAtualizacaoStatus(Status statusAtualizacao, Status statusAtual)
        {

            if (statusAtual == Status.AguardandoPagamento && statusAtualizacao == Status.PagamentoAprovado
            || statusAtual == Status.AguardandoPagamento && statusAtualizacao == Status.Cancelada)
            {
                return true;
            }

            if (statusAtual == Status.PagamentoAprovado && statusAtualizacao == Status.EnviadoTransportador
            || statusAtual == Status.PagamentoAprovado && statusAtualizacao == Status.Cancelada)
            {
                return true;
            }

            if (statusAtual == Status.EnviadoTransportador && statusAtualizacao == Status.Entregue)
            {
                return true;
            }

            return false;
        }

    }
}
