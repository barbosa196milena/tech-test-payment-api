namespace tech_test_payment_api.Enums
{
    public enum Status
    {
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoTransportador,
        Cancelada,
        Entregue
    }
}