using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        public Vendendor Vendendor { get; set; }
        public DateTime Data { get; set; }
        public int IdPedido { get; set; }
        public List<string> Produto { get; set; }
        public int Status { get; set; }

        public Venda(){}
    }
}